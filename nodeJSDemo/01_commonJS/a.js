
function test(){
    console.log('aaa')
}
function upper(str){
    return str.substring(0,1).toUpperCase()+str.substring(1)
}
// 导出不能写多个，只能写一个，不然后面的会把前面的覆盖掉
// module.exports=test
// module.exports=upper

module.exports={
    test,upper
}

// 这种可以写多个
// exports.test=test
// exports.upper=upper