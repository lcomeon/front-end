var http=require("http")
const { url } = require("inspector")

http.createServer((req,res)=>{
    var urlobj=url.parse(req.url,true)
    console.log(urlobj.query.callback)
    switch(urlobj.pathname){
        case'api/aaa':
        res.end(`${urlobj.query.callback}(${JSON.stringify({
            name:"limiao",
            age:100
        })})`)
        break;
        default:
            res.end('404')
    }
}).listen(3000)














// JSONP(JSON with Padding)是一种跨域通信的技术，通过动态创建<script>标签，使用<script>标签的跨域特性，来实现跨域通信
// 实现原理在客户端创建一个<script>标签，其src属性指向服务端的API接口地址，并通过URL传递一个回调参数的名称。
// 服务端在返回数据时,将数据作为参数传递给回调函数,并将整个回掉函数的调用作为响应内容返回给客户端.客户端在接收到响应后,会自动执行回调函数,从而获取到服务端返回的数据

// 优点:兼容性好,可以在大部分浏览器中使用且实现简单,但是,JSONP也存在一些缺点,只能支持get请求,不支持post请求等.另外由于是动态创建的<script>标签，
// 因此需要服务端的支持。另外由于是通过URL参数传递回调函数名称的，因此可能存在安全问题,容易受到XSS供给,因此在使用时需要谨慎处理数据安全问题