var http = require("http")
var https = require("https")
const url = require("url")

http.createServer((req, res) => {
    var urlobj = url.parse(req.url, true)
    console.log(urlobj.query.callback)

    res.writeHead(200, {
        "Content-Type": "application/json;charset=utf-8",
        "access-control-allow-origin": "*"
    })
    switch (urlobj.pathname) {
        case '/api/aaa':
            //客户端  去要数据
            httpget((data)=>{//这个函数不会立即执行，在收集完数据后
                res.end(data)
            })
            break;
        default:
            res.end('404')
    }
}).listen(3000)


function httpget(cb) {
    var data = ""
    // http.get()  //可以请求http协议的
    https.get("https://i.maoyan.com/api/mmdb/movie/v3/list/hot.json?ct=%E7%A7%A6%E7%9A%87%E5%B2%9B&ci=122&channelId=4", (res) => {
        res.on("data", (chunk) => {  //一点一点返回数据
            data += chunk
            console.log('返回数据')

        })
        res.on("end", () => {//全部返回数据
            console.log(data)
            // response.end(data)
            cb(data)//形参cd就是httpget调用里的箭头函数，给cd传data就是给箭头函数传的
        })
    })

}

//node能够创建服务器给前端提供接口、代码片段
// node也可以作为客户端向别的服务器要数据，并且再给到前端







