var fs = require("fs").promises


fs.readdir("./avatar").then(async (data)=>{
    await Promise.all(data.map(item=>fs.unlink(`./avatar/${item}`)
    ))
    await fs.rmdir("./avatar")
})

// fs.readdir("./avatar").then(async (data) => {
//     let arr=[]
//     data.forEach(item => {
//         arr.push(fs.unlink(`./avatar/${ item }`))
//     })
//    await Promise.all(arr)  //所有的异步都执行完之后
//    await fs.rmdir("./avatar")
// })

// unlinkSync同步保证要删除的文件夹下所有文件全部删除，在执行下面的代码
// fs.readdir("./avatar",(err,data)=>{
//     console.log(data)
//     data.forEach(item=>{
//         fs.unlinkSync(`./avatar/${item}`)
//     })
//     fs.rmdir("./avatar",(err)=>{
//         console.log(err)
//     })
// })

// 在fs模块中 提供同步方法是为了方便使用，那到底是用同步还是异步呢
// 由于Node环境执行的js代码是服务器端代码，所以绝大部分需在服务器运行期反复执行业务逻辑的代码
// 必须使用异步代码，否则同步代码在执行时期，服务器将停止响应，因为js只有一个执行线程

// 服务器如果需要读取配置文件，或者结束时需要写到状态文件时，可以使用同步代码，因为这些代码只在启动和结束时执行一次，不影响服务器正常运行时的异步执行