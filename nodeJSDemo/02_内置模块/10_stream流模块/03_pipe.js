const fs = require("fs")

const readStream=fs.createReadStream("./1.txt")

const writeStream=fs.createWriteStream("./2.txt")

// 高性能复制文件的方法
readStream.pipe(writeStream)