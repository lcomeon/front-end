const crypto=require("crypto")

const hash =crypto.createHash("md5")
// const hash =crypto.createHash("sha1")
// 可任意多次调用update():
hash.update("hello world")
hash.update("hello world222")

console.log(hash.digest("hex"))




// `update()`方法默认字符串编码为`UTF-8`，也可以传入Buffer。

// 如果要计算SHA1，只需要把`'md5'`改成`'sha1'`，就可以得到SHA1的结果`1f32b9c9932c02227819a4151feed43e131aca40`。




// crypto 模块的目的是为了提供通用的加密和哈希算法，用纯js代码实现这些功能不是不可能，但速度会非常慢。
// Nodejs用C/C++实现这些算法后，通过crypto这个模块暴露为js接口，这样用起来方便，运行速度也快

// MD5是一种常用的哈希算法，用于给任意一个数据签名，这个签名通常用一个十六进制的字符串表示