const crypto=require("crypto")

function encrypt(key,iv,data){
    let dep=crypto.createCipheriv("aes-128-cbc",key,iv)
    return dep.update(data,'binary',"hex") + dep.final("hex")
}

function decrypt(key,iv,cryted){
 cryted=Buffer.from(cryted,"hex").toString("binary")
    let dep=crypto.createDecipheriv("aes-128-cbc",key,iv)
    return dep.update(cryted,'binary','utf-8') +dep.final("utf-8")
}

let key="abcdef1234567890"
let iv='abcdef1234567890'
let data="limiao"

let cryted=encrypt(key,iv,data)
let decryted=decrypt(key,iv,cryted)
console.log('加密结果',cryted)
console.log('解密结果',decryted)
