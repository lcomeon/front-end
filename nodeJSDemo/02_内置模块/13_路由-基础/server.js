const http = require("http")
const route=require("./router")
http.createServer((req, res) => {
    const myURL = new URL(req.url, "http://127.0.0.1")
    route(res,myURL.pathname)
    res.end()
}).listen(3000, () => {
    console.log("server start")
})