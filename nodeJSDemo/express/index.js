const express = require("express")

const app = express()

app.get("/", (req, res) => {
    // res.write("hello")
    // res.end()
    // send可以代替 write和end  可以解析片段
    // res.send("nihao")
    // res.send(
    //     `<div>哈哈哈哈</div>`
    // )
    // send  代码片段，接口，js数据都可以传
    // 不需要设置响应头
    res.send({
        name: "limiao",
        age: 90
    })
})
// b可以有也可以没有  /abcd  /acd 都可以成功返回ok 
app.get("/ab?cd", (req, res) => {
    res.send("ab?cd")
})

// 详情页  /ab/随机
app.get("/ab/:id", (req, res) => {
    res.send("ab/:id")
})
//  可以匹配 abcd  abbcd abbbcd 等  b可重复
app.get("/ab+cd", (req, res) => {
    res.send("/ab+cd")
})
//  可以在b  和  c中间输入任意字符
app.get("/ab*cd", (req, res) => {
    res.send("/ab*cd")
})
//  abe  abcde
app.get("/ab(cd)?e", (req, res) => {
    res.send("/ab(cd)?e")
})

// 匹配任何路径中含有 a 的路径：
app.get(/a/, function (req, res) {
    res.send('/a/');
});

//  使用正则表达式
//必须以fly结尾  匹配 butterfly、dragonfly，不匹配 butterflyman、dragonfly man等
app.get(/.*fly$/, function (req, res) {
    res.send('/.*fly$/');
});
app.listen(3000, () => {
    console.log("server start")
})

// 可以为请求处理提供多个回调函数，其行为类似中间件
// next判断是否走下一个中间件
app.get("/home", (req, res,next) => {
    console.log('验证')
    if(true){
        next()
    }else{
        // 返回错误
        res.se
    }
}, (req, res) => {
    res.send()
})

// 使用回调函数数组处理路由
var cb0=function(req,res,next){
    console.log('cb0')
    res.lm="这是cb0的结果"
    next()
}
var cb1=function(req,res,next){
    console.log('cb1')
    console.log(res.lm)
    next()
}
var cb2=function(req,res,next){
    console.log('cb2')
}
app.get("./home",[cb0,cb1,cb2])

// 混合使用函数和函数数组处理路由：
var cb0 = function (req, res, next) {
    console.log('CB0')
    next()
  }
  
  var cb1 = function (req, res, next) {
    console.log('CB1')
    next()
  }
  
  app.get('/example/d', [cb0, cb1], function (req, res, next) {
    console.log('response will be sent by the next function ...')
    next()
  }, function (req, res) {
    //send一般都在最后一个函数中
    res.send('Hello from D!')
  })