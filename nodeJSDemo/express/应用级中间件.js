const express = require("express")

const app = express()

//比如每个函数在请求数据前都要先校验token是否存在，不妨把校验token这个函数抽离出来，这样就不用在每次请求数据前都再次调用
// app.use  
var cb0=function(req,res,next){
    console.log('cb0')
    res.lm="这是cb0的结果"
    next()
}
//中间件的注册需要顺序 比如检验token，还没登录时就不需要中间件，校验token的中间件就必须在登录后执行
app.use(cb0)
app.use( "/home",cb0) //只响应这个页面的中间件
var cb1=function(req,res,next){
    console.log('cb1')
    console.log(res.lm)
    next()
}
var cb2=function(req,res,next){
    console.log('cb2')
}
// app.get("./home",[cb0,cb1,cb2])
app.get("./home",[cb1,cb2])