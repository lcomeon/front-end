const express = require("express")
const app = express()
const IndexRouter = require("./route")


// 应用级别
app.use(function (req, res, next) {
    console.log('应用级中间件')
    next()
})

// 应用级别
app.use("/", IndexRouter)

app.listen(3000,()=>{
    console.log("server")
})
