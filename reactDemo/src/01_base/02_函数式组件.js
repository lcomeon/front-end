import React from "react";
// 16.8之前函数式组件是无状态组件
// 16.8之后引入react hooks就成了有状态组件
function App(){
    return (<div>hello</div>)
}

export default App