import React, { Component } from 'react'
import './css/01_index.css' //导入css， webpack的支持

export default class App extends Component {
  render() {
    var myname='lmcome'
    var bg={
        background:'yellow',
        fontSize:'36px'
        // backgroundColor:'yellow' //带连接线的改成驼峰写法
    }
    return (
      <div style={bg}>
        {10+20}-{myname}
        {10>20 ?'aaa':'bbb'}
        <div className='active'>
            haha
        </div>
        {/* <label for='username' >用户名：</label><input type='text' id='username' /> */}
        <label htmlFor='username' >用户名：</label><input type='text' id='username' />
      </div>
    )
  }
}

// 组件的样式

// 1.行内样式：需使用表达式传入样式对象的方式来实现
// 行内样式需要写入一个样式对象，这个样式对象的位置可以放在很多地方，例如render函数，组件原型，外链js文件中
// 注意：这两个括号，第一个表示要在JSX里插入JS，第二个是对象的括号
//  <p style={{color:'red',fontSize:'19px'}}>Hello</p> 

// 2.使用class
// React推荐我们使用行内样式，因为React觉得每一个组件都是一个独立的整体
// 其实大多数情况下还是在为元素添加类名
//  <p className='类名'>hello</p> 

// class ==>className  for==>htmlFor(label)
