import React, { Component } from 'react'

export default class App extends Component {
  a=100
  render() {
    return (
      <div>
        <input type="text" />
        <button onClick={() => {
          console.log('11',this.a)
          
        }}>不推荐</button>

        {/* <button onClick={this.handleClick()}>add</button> */}
        <button onClick={this.handleClick}>推荐</button>
        <button onClick={this.handleClick2.bind(this)}>不推荐 handleClick2</button>
        <button onClick={()=>{
          this.handleClick1()
        }}>比较推荐  传参</button>
      </div>
    )
  }
  handleClick(){
    console.log('handleClick')
    console.log(this)//输出undefined
  } 
  handleClick1 =()=>{
    console.log('handleClick1')
    console.log(this)
  }
  handleClick2(){
    console.log(this)//正常输出this
  }
}

// on**   on是固定的后边是驼峰写法

// 改变this指向

// 箭头函数this有保障

// 1.直接在render行内写箭头函数  不推荐
// 2.在组件内使用箭头函数定义一个方法  推荐
// 3.直接在组件内定义一个非箭头函数的方法，然后在render里直接使用onClick=this.handleClick.bind(this)}  不推荐
// 4.直接在组件内定义一个非箭头函数方法，然后在constructor里bind(this) 推荐
// React并不会真正绑定事件到每一个具体元素身上，而是采用事件代理的模式