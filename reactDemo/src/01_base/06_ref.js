import React, { Component } from 'react'

export default class App extends Component {
  a=100
  myref=React.createRef()

  render() {
    return (
      <div>
        <input ref="mytext" />
        <input ref={this.myref} />
        <button onClick={()=>{
          console.log('click1',this.refs.mytext.value)
          console.log('click1',this.myref.current.value)
        }}> add1</button>
        <button onClick={this.handleClick}>handleClick</button>
        <button onClick={this.handleClick1}>handleClick1</button>
      </div>
    )
  }
  handleClick(){
    console.log(this.myref.current.value)//报错
  }
  handleClick1 =()=>{
    console.log(this.myref.current.value)//成功输出
  }
}


// ref的应用
// 给标签设置ref='username'  通过这个获取 this.refs.username  ref可以获取到应用的真实dom
// 给组件设置 ref='username'  通过这个获取 this.refs.username  ref可以获取到组件对象
// 新的写法  
