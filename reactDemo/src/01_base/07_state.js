import React, { Component } from 'react'

export default class App extends Component {
  // 写法一：
  //state固定写法
  // state={
  //   text:'收藏',
  //   isShow:true
  // }

  // 写法二：
  constructor(){
    super()
    this.state={
      text:'收藏',
      isShow:true,
      myname:'lmcome'
    }
  }
  render() {
    return (
      <div>
        欢迎 -{this.state.myname}
        <br />
        <button onClick={()=>{
          // this.state.text= '取消'//不要直接改
          this.setState({
            isShow:!this.state.isShow,
            myname:'李淼'
          })
          if(this.state.isShow){
            console.log('收藏')
          }else{
            console.log('取消收藏')
          }
        }}>{this.state.isShow ? '收藏' : '取消收藏'}</button>
      </div>
    )
  }
}

// 状态  组件描述某种显示情况的数据，由组件自己设置和更改，使用状态的目的就是为了在不同状态下使组件的显示不同