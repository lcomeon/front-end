import React, { Component } from 'react'

export default class App extends Component {
  state={
    list:[11,222,333]
  }
  render() {
    return (
      <div>
        <ul>
          {
            this.state.list.map(item=>
              <li>{item}</li>
              )
          }
        </ul>
      </div>
    )
  }
}

// 原生js  map  映射
// var list =['aa','bb','cc']
// var newlist=list.map(item=>'lm')
// console.log(newlist) //输出 ['lm', 'lm', 'lm']

// var list =['aa','bb','cc']
// var newlist=list.map(item=>`<li>${item}</li>`)
// console.log(newlist.join('')) //输出 <li>aa</li><li>bb</li><li>cc</li>