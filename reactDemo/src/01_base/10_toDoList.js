import React, { Component } from 'react'
export default class App extends Component {
  a = 100
  myref = React.createRef()
  state = {
    list: [
      {
        id: '1',
        myname: '假直播',
      },
      {
        id: '2',
        myname: '假直播2',
      },
      {
        id: '3',
        myname: '假直播3',
      }
    ]
  }

  render() {
    return (
      <div>
        <input ref={this.myref} />
        <button onClick={this.handleClick1}>handleClick1</button>
        <ul>
          {this.state.list.map((item, index) => <li key={item.id}>{item.myname}

          {/* 富文本展示 */}
          <span dangerouslySetInnerHTML={{__html:item.myname}}></span>

            <button onClick={() => this.handleClick(index)}>handleClick</button>
          </li>)}
        </ul>
        {/* {this.state.list.length ===0 ? <div>
          暂无
        </div> :null} */}


{/* div的创建和删除 */}
        {this.state.list.length ===0 && <div>暂无数据</div>}
        <div className ={this.state.list.length===0 ? '' :'hidden'}>暂无待办事项</div>
      </div>
    )
  }
  handleClick = (index) => {
    console.log('当前index是', index)
    let newlist = this.state.list.concat()
    newlist.splice(index, 1)
    this.setState({
      list: newlist
    })
  }
  handleClick1 = () => {
    console.log(this.myref.current.value)//成功输出

    // 不要直接修改状态，可能会造成不可预期的问题
    // this.state.list.push(this.myref.current.value)
    let newlist = this.state.list
    newlist.push(
      {
        id: Math.floor(Math.random()),
        myname: this.myref.current.value
      }
    )
    this.setState({
      list: newlist
    })

    // 清空输入框

  }
}