import React, { Component } from 'react'

export default class App extends Component {
    state = {
        count: 1
    }
    render() {
        return (
            <div>
                {this.state.count}
                <button onClick={this.handleClick}>add</button>
                <button onClick={this.handleClick1}>add1</button>
            </div>
        )
    }
    handleClick = () => {
        this.setState({
            count: this.state.count + 1
        }, () => {
            console.log(this.state.count) //第一次输出1
        })
        this.setState({
            count: this.state.count + 1
        }, () => {
            console.log(this.state.count) //第一次输出1
        })
        this.setState({
            count: this.state.count + 1
        }, () => {
            console.log(this.state.count) //第一次输出1
        })
    }
    handleClick1 = () => {
        setTimeout(() => {
            this.setState({
                count: this.state.count + 1
            })
            console.log(this.state.count) //第一次输出1
            this.setState({
                count: this.state.count + 1
            })
            this.setState({
                count: this.state.count + 1
            })
        }, 1000);
    }
}
//setState异步更新：
// setState处在同步逻辑中，则异步更新状态和DOM

// setState同步更新：
// setState处在异步逻辑中，则同步更新状态和DOM

// setState接收第二个参数，为参数式回调函数，状态和dom更新后就会被触发