import React, { Component } from 'react'
import BetterScroll from 'better-scroll'

export default class APP extends Component {
    state = {
        list: []
    }
    render() {
        return (
            <div>
                <button onClick={() => this.getData()}>展示</button>
                <div className="wrapper" style={{ height: '200px', background: 'gray', overflow: 'hidden' }}>
                    <ul className="content">
                        {this.state.list.map(item => <li key={item}>{item}</li>)}
                    </ul>
                </div>
            </div>

        )
    }
    getData() {
        var list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
        this.setState({
            list: list
        }, () => {
            //回调函数知道什么时候DOM上树
            console.log(this.state.list)
            console.log(document.querySelectorAll('li'))
            new BetterScroll(".wrapper")
        })
        //此时DOM没上树
        // console.log(this.state.list)
        // console.log(document.querySelectorAll('li'))

    }

}
