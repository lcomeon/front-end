import React, { Component } from 'react'
import Navbar from './Navbar'
export default class App extends Component {
    render() {
        return (
            <div>
                <div>
                    <h3>首页</h3>
                    <Navbar title='首页' leftshow={true}/> {/**  leftshow='false'  传的是普通字符串不是bool值 */}
                </div>
                <div>
                    <h3>列表</h3>
                    <Navbar title='列表'/>
                </div>
            </div>
        )
    }
}
