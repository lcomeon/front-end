import React, { Component } from 'react'
import axios from 'axios'

export default class Cinema extends Component {
  constructor() {
    super()
    this.state = {
      cinemaList: [],
      text:""
    }
    // axios.get('请求地址').then(res=>{}).catch(err=>{})  基于ES6的promise进行封装的

    // 请求数据  生命周期函数更适合发送请求
    axios.get('https://m.maizuo.com/gateway?cityId=110100&ticketFlag=1&k=7406159').then(res => {
      console.log('查询到了', res)
      this.setState({
        cinemaList: res.data.data.cinemas,
      })
    }).catch(err => {
      console.log('请求失败', err)
    })
  }
  render() {
    return (
      <div>
        <input value={this.state.text} onChange={(evt)=>{
          this.setState({
            text:evt.target.value
          })
        }}/>
        {
         this.getCinemaList().map(item =>
            <dl key={item.cinemaId}>
              <dt>{item.name}</dt>
              <dd>{item.address}</dd>
            </dl>
          )
        }
      </div>
    )
  }
  getCinemaList(){
    return this.state.cinemaList.filter(item => item.name.toUpperCase().includes(this.state.text.toUpperCase()) ||
    item.address.toUpperCase().includes(this.state.text.toUpperCase()))
  }
}
// filter
var arr = ['11', '2', '3']
// var newArr=arr.filter(item=>true)
var newArr = arr.filter(item => item.includes('2'))//返回true能过滤出来false不过滤
console.log(newArr)