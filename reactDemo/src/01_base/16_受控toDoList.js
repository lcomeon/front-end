import React, { Component } from 'react'
export default class App extends Component {
  a = 100
  state = {
    text:'',
    list: [
      {
        id: '1',
        myname: '假直播',
        isChecked:false
      },
      {
        id: '2',
        myname: '假直播2',
        isChecked:false
      },
      {
        id: '3',
        myname: '假直播3',
        isChecked:true
      }
    ]
  }

  render() {
    return (
      <div>
        <input value={this.state.text} onChange={(evt)=> this.setState({
          text:evt.target.value
        })} />
        <button onClick={this.handleClick1}>add</button>
        <ul>
          {this.state.list.map((item, index) => <li key={item.id}>{item.myname}


          <input type="checkbox" checked={item.isChecked} onChange={()=>this.handelChecked(index)} />
          {/* 富文本展示 */}
          {/* <span dangerouslySetInnerHTML={{__html:item.myname}}></span> */}

            <button onClick={() => this.handleClick(index)}>del</button>
          </li>)}
        </ul>
        {/* {this.state.list.length ===0 ? <div>
          暂无
        </div> :null} */}


{/* div的创建和删除 */}
        {this.state.list.length ===0 && <div>暂无数据</div>}
        <div className ={this.state.list.length===0 ? '' :'hidden'}>暂无待办事项</div>
      </div>
    )
  }
  handleClick = (index) => {
    console.log('当前index是', index)
    let newlist = this.state.list.concat()
    newlist.splice(index, 1)
    this.setState({
      list: newlist
    })
  }
  handleClick1 = () => {
    console.log(this.myref.current.value)//成功输出

    // 不要直接修改状态，可能会造成不可预期的问题
    // this.state.list.push(this.myref.current.value)
    let newlist = [...this.state.list]
    newlist.push(
      {
        id: Math.floor(Math.random()),
        myname:this.state.text,
        isChecked:false
      }
    )
    this.setState({
      list: newlist,
      text:''
    })

    // 清空输入框

  }
  handelChecked(index){
    let newlist = [...this.state.list]
    newlist[index].isChecked=!newlist[index].isChecked
    this.setState({
      list:newlist
    })


  }
}