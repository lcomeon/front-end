import React, { Component } from 'react'
import Prop from 'prop-types'

export default class Navbar extends Component {
    state = {
        // 只能内部自己用，外面无法改变
    }
    // 属性是父组件传来的，this.props
    render() {
        console.log(this.props)
        let { title,leftshow } = this.props
        // 接收属性的时候做验证
        return (
            <div>
                {/* {this.props.title} */}
               {leftshow &&  <button>返回</button>}
                navbar - {title}
                <button>home</button>
            </div>
        )
    }
}
// 类属性
Navbar.propTypes={
    title: Prop.string,
    leftshow:Prop.bool
}



class Test{
    a=1
}
Test.a=100