import React, { Component } from 'react'


class Navbar extends Component{
    render(){
        return(
            <div style={{background:'red'}}>
                <button  onClick={()=>this.props.event()}>click</button>
                <span>navbar</span>
            </div>
        )
    }
}
class Slider extends Component{
    render(){
        return(
            <div style={{background:'yellow'}}>
                <ul>
                    <li>111</li>
                    <li>111</li>
                    <li>111</li>
                    <li>111</li>
                    <li>111</li>
                    <li>111</li>
                    <li>111</li>
                </ul>
            </div>
        )
    }
}

export default class App extends Component {
    state={
        isShow:true
    }
  render() {
    return (
      <div>
        <Navbar event={this.handle} />
       {this.state.isShow && <Slider />}
      </div>
    )
  }
  handle=()=>{
    this.setState({
        isShow:!this.state.isShow
    })
    console.log('调用父组件了')
  }
}

// state 状态只能在组件内部使用   不管父子 不管兄弟  
// 父传子  通过属性
// 子传父  通过回调函数 callback