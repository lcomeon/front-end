import React, { Component } from 'react'
import './css/02_maizuo.css' //导入css， webpack的支持
import Film from './checktab01/Film'
import Cinema from './checktab01/Cinema'
import Center from './checktab01/Center'
import Tabbar from './checktab01/Tabbar'
import Navbar from './checktab01/Navbar'

export default class App extends Component {
    state = {
        current: 0
    }

    render() {
        return (
            <div>
                <Navbar event={this.navbarClick} />
                {/* {this.state.current==0 &&  <Film></Film>}
                {this.state.current==1 &&  <Cinema></Cinema>}
                {this.state.current==2 &&  <Center></Center>} */}
                {this.pickOne()}
                <Tabbar event={this.handleClick} current={this.state.current}/>
            </div>
        )
    }
    pickOne() {
        // if、switch
        switch (this.state.current) {
            case 0:
                return <Film></Film>
            case 1:
                return <Cinema></Cinema>
            case 2:
                return <Center></Center>
            default:
                return null
        }
    }
    handleClick = (index) => {
        console.log(index)
        this.setState({
            current: index
        })

        //通知父组件该修改父组件的状态了
    }
    navbarClick = () => {
        this.setState({
            current: 2
        })

        //通知父组件该修改父组件的状态了
    }
}
