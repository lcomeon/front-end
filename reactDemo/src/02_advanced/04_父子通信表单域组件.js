import React, { Component } from 'react'


class Field extends Component {
    render() {
        return (
            <div style={{ background: 'yellow' }}>
                <label>{this.props.label}</label>
                <input type={this.props.type} value={this.props.value} onChange={(evt) => {
                    this.props.onChangeEvent(evt.target.value)
                }} />
            </div>
        )
    }
}
export default class App extends Component {
    state = {
        username: '',
        password: ''
    }
    render() {
        return (
            <div>
                <h1>登录页面</h1>
                <Field label="用户名" type="text" value={this.state.username} onChangeEvent={(value) => {
                    console.log(value)
                    this.setState({
                        username: value
                    })
                }} />
                <Field label="密码" type="password" value={this.state.password} onChangeEvent={(value) => {
                    console.log(value)
                    this.setState({
                        password: value
                    })
                }} />
                <button onClick={() => {
                    console.log(this.state.username)
                    console.log(this.state.password)
                }}>登录</button>
                <button onClick={() => {
                    this.setState({
                        username: '',
                        password: ''
                    })
                }}>重置</button>
            </div>
        )
    }
}
