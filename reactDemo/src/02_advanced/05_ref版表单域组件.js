import React, { Component } from 'react'


class Field extends Component {
    state = {
        value: ''
    }
    // 内部清空
    clear(){
        this.setState({
            value:''
        })
    }
    // 外部传值设置
    setValue(value){
        this.setState({
            value:value
        })
    }
    render() {
        return (
            <div style={{ background: 'yellow' }}>
                <label>{this.props.label}</label>
                <input type={this.props.type} onChange={(evt) => {
                    this.setState({
                        value: evt.target.value
                    })
                }}  value={this.state.value} />
            </div>
        )
    }
}
export default class App extends Component {
    username = React.createRef()
    password = React.createRef()
    render() {
        return (
            <div>
                <h1>登录页面</h1>
                <Field label="用户名" type="text" ref={this.username} />
                <Field label="密码" type="password" ref={this.password} />
                <button onClick={()=>{
                    console.log(this.username.current.state.value)
                    console.log(this.password.current.state.value)
                }}>登录</button>
                <button onClick={()=>{
                    this.username.current.clear()
                    this.password.current.clear()
                }}>重置</button>
            </div>
        )
    }
}
