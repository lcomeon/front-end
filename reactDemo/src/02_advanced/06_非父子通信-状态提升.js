import React, { Component } from 'react'
import axios from 'axios'
import './css/03_communication.css'
export default class App extends Component {
    constructor() {
        super()
        this.state = {
            filmList: [],
            info: []
        }
        axios.get('/test.json').then(res => {
            this.setState({
                filmList: res.data.data.films
            })
            console.log(res.data.data.films)
        })


    }
    render() {
        return (
            <div>
                {/* <FilmItem />
         */}
                {this.state.filmList.map((item) => {
                    <FilmItem key={item.filmId} {...item}
                        onEvent={(value) => {
                            console.log('在父组件接收', value)
                            this.setState({
                                info:value
                            })
                        }} />
                })}
                <FilmDetail info={this.state.info}/>
            </div>
        )
    }
}
//受控组件
class FilmItem extends Component {
    render() {
        let { name, poster, synopsis } = this.props
        return (
            <div className='filmItem' onClick={() => {
                console.log(synopsis)
                //调用父组件
                this.props.onEvent(synopsis)
            }}>
                <img src={poster} alt={name} />
                <h4>{name}</h4>
            </div>

        )
    }
}
class FilmDetail extends Component {
    render() {
        return (
            <div className='filmdetail'>
                {this.props.info}
            </div>
        )
    }
}

// 状态提升-中间人模式
// 将多个组件需要共享的状态提升到它们最近的父组件上，在父组件上改变这个状态然后通过props分发给子组件