import React, { Component } from 'react'

export default class APP extends Component {
    render() {
        return (
            <div>

            </div>
        )
    }
}


var bus = {
    list: [],
    // 订阅
    subscribe(callback) {
        this.list.push(callback)
    },
    // 发布
    publish(value) {
        //遍历所有的list，将回调函数回调
        this.list.forEach(callback => {
            callback && callback(value)
        })



    }
}

// 订阅者  
bus.subscribe((value) => {
    console.log('111',value)

})
bus.subscribe((value) => {
    console.log('222',value)
})

//发布者一定要更晚的发布
setTimeout(() => {
    bus.publish('发布内容')
}, 0);

// Redux  基于发布订阅

//发布订阅模式

// 发布者
//   ||
//   ||  微信公众号 有两个函数（订阅函数，发布函数）（让回调函数被执行）
//   ||
// 订阅者（早早的订阅这个公众号，把回调函数交给公众平台）