import React, { Component } from 'react'
import axios from 'axios'
import './css/03_communication.css'


var bus = {
    list: [],
    // 订阅
    subscribe(callback) {
        this.list.push(callback)

    },
    // 发布
    publish(value) {
        this.list.forEach(callback=>{
            callback && callback(value)
        })

    }
}

export default class App extends Component {
    constructor() {
        super()
        this.state = {
            filmList: [],
        }
        axios.get('/test.json').then(res => {
            this.setState({
                filmList: res.data.data.films
            })
            console.log(res.data.data.films)
        })


    }
    render() {
        return (
            <div>
                {this.state.filmList.map((item) => {
                    <FilmItem key={item.filmId} {...item}
                    />
                })}
                <FilmDetail />
            </div>
        )
    }
}
//发布者
class FilmItem extends Component {
    render() {
        let { name, poster, synopsis } = this.props
        return (
            <div className='filmItem' onClick={() => {
                console.log(synopsis)
                // 进行发布
                bus.publish(synopsis)
            }}>
                <img src={poster} alt={name} />
                <h4>{name}</h4>
            </div>

        )
    }
}

// 订阅者
class FilmDetail extends Component {
    constructor(){
        super()

        this.state={
            info:''
        }
        // 进行订阅
        bus.subscribe((info)=>{
            console.log('我在filmdetail定义')
            this.setState({
                info:info
            })
        })
    }
    render() {
        return (
            <div className='filmdetail'>
                {this.state.info}
            </div>
        )
    }
}
