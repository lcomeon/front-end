import React, { Component } from 'react'
import axios from 'axios'
import './css/03_communication.css'

const GlobalContext = React.createContext()//创建Context对象


export default class App extends Component {
    constructor() {
        super()
        this.state = {
            filmList: [],
            info:''
        }
        axios.get('/test.json').then(res => {
            this.setState({
                filmList: res.data.data.films
            })
            console.log(res.data.data.films)
        })


    }
    render() {
        return (
            // 供应商组件  必须写成value
            <GlobalContext.Provider value={{
                call: 'call',
                sms: 'sms',
                info:this.state.info,
                changeInfo:(value)=>{
                    this.setState({
                        info:value
                    })
                }
            }}>
                <div>
                    {this.state.filmList.map((item) => {
                        <FilmItem key={item.filmId} {...item}
                        />
                    })}
                    <FilmDetail />
                </div>
            </GlobalContext.Provider>
        )
    }
}
//受控组件
class FilmItem extends Component {
    render() {
        let { name, poster, synopsis } = this.props
        return (
            <GlobalContext.Consumer>
                {
                    (value) => {
                        console.log(value)
                        return <div className='filmItem' onClick={() => {
                        // value.info='222'
                        value.changeInfo(synopsis)
                        console.log(synopsis)
                        }}>
                            <img src={poster} alt={name} />
                            <h4>{name}</h4>
                        </div>
                    }
                }

            </GlobalContext.Consumer>


        )
    }
}
class FilmDetail extends Component {
    render() {
        return (
            <GlobalContext.Consumer>
                {
                    (value) =>  <div className='filmdetail'>
                            detail-{value.call}
                        </div>

                }
            </GlobalContext.Consumer>

        )
    }
}

// 哪两个兄弟组件想通信
// 将最外层组件做成供应商组件
// 里边的子组件谁想通信就作为消费者

// 生产者

// 消费者