import React, { Component } from 'react'

class Child extends Component{
    render(){
        return(
            <div>child
                {/* 插槽 */}
                {this.props.children[0]}
                {this.props.children[1]}
                {this.props.children[2]}

            </div>
        )
    }
}
export default class App extends Component {
  render() {
    return (
      <div>
        <Child>
           {/* <div>999</div> 不能正常显示，只能显示child组件里的内容 */} 
           <div>999</div>
           <div>22</div>
           <div>33</div>
        </Child>
      </div>
    )
  }
}


// 为了复用组件  比如复用轮播图组件但是所要轮播的内容不一样则可以用插槽
// 一定程度减少父子通信
