

import React, { Component } from 'react'

export default class App extends Component {
    // 已丢弃  将要挂载  只走一次
    componentWillMount() {
        console.log('will')

        // 上树前最后一次修改机会

    }
    // 已经挂载 只走一次
    componentDidMount() {
        // 数据请求、订阅函数调用、setInterval、基于创建完的dom进行初始化   BetterScroll
        console.log('did')
    }
    //正在渲染   不止一次，状态更新也要渲染
    render() {
        console.log('render')
        return (
            <div>

            </div>
        )
    }
}

// 类的组件的生命周期
// componentWillMount   render之前最后一次修改状态的机会
// render  只能访问this.props和this.state   不允许修改状态和DOM输出
// componentDidMount  渲染完成真实DOM之后触发，可以修改DOM