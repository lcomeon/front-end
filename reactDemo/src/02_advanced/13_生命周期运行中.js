import React, { Component } from 'react'

class Child extends Component {
    //已丢弃  只能应用在子组件中   就算不传属性父组件更新也会走这里 
    //   最先获得父组件传来的属性，可以利用属性进行ajax或者逻辑处理
    // 把属性值转换成子组件自己的状态
    componentWillReceiveProps(nextProps) {
        console.log('componentWillReceiveProps')
        // this.props.text  拿到的是旧的属性
        // nextProps  才是新的属性
        // console.log()
        // window.print()

    }
    render() {
        return (
            <div>111</div>
        )
    }
}
export default class App extends Component {
    state = {
        myname: 'limiao'
    }

    // 每次状态更新的时候才会对比
    shouldComponentUpdate(nextProps, nextState) {
        // return true应该更新
        // return false阻止更新
        // this.state  老状态
        // nextState 新状态
        if (this.state.myname !== nextState.myname) {
            return true
        }
        if (JSON.stringify(this.state) !== JSON.stringify(nextState.myname)) { //不能直接对比对象，因为两个对象永远不可能相等，可以转换成字符串进行对比
            return true
        }
        return false
    }

    //已丢弃  不能修改属性和状态  不止执行一次
    componentWillUpdate() {
        console.log('componentWillUpdate')
    }
    // 只能访问this.props和this.state   不允许修改状态和DOM输出
    render() {
        console.log('render')
        return (
            <div>
                <button onClick={() => {
                    this.setState({
                        myname: '李淼'
                    })
                }}>click</button>
                {this.state.myname}

                <Child text={this.state.myname} />
            </div>
        )
    }
    // 已经更新完了  唯一的缺陷是执行多次，可以自己加判断控制执行次数
    componentDidUpdate(prevProps, prevState) {
        // if(prevState.)
        console.log('componentDidUpdate')
    }
}


// 生命周期：
// componentWillReceiveProps  已弃用 放在子组件中
// componentWillUpdate   不能修改属性和状态  已弃用
// render
// componentDidUpdate   可以修改DOM
// shouldComponentUpdate