import React, { Component } from 'react'

class Box extends Component{
  
    shouldComponentUpdate(nextProps){
        // this.props.current ===this.props.index 旧的比
        // nextProps.current === nextProps.index  新的比
        // 减少render更新次数，再次输入只更新上一次选中的和最新选中的
        if(this.props.current ===this.props.index || nextProps.current === nextProps.index){
            return true
        }
        return false
    }
    render(){
        console.log('render')
        return(
            <div style={{width:"100px",height:"100px",border:this
            .props.current===this.props.index ? '1px solid red' : '1px solid gray' }}>
            </div>
        )
    }
}
export default class App extends Component {
    state={
        list:['00','01','03','04','05','06','07'],
        current:0
    }
  render() {
    return (
      <div>
        <input type='number' onChange={(e)=>
            this.setState({
                current:Number(e.target.value)
            })
        } />
        <div style={{overflow:'hidden'}}> 
        {
            this.state.list.map((item,index)=>
                <Box key={item} current={this.state.current} index={index}/>
            )
        }
        
        </div>
      </div>
    )
  }
}
