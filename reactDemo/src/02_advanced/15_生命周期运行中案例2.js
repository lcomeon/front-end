import React, { Component } from 'react'

export default class App extends Component {
    state={
        type:1
    }
  render() {
    return (
      <div>
        <ul>
            <li onClick={()=>{
                this.setState({
                    type:1
                })
            }}>正在热映</li>
            <li onClick={()=>{
                this.setState({
                    type:2
                })
            }}>即将上映</li>
        </ul>
        <FilmList type={this.state.type}/>
      </div>
    )
  }
}

class FilmList extends Component{
    // 初始化声明周期  只执行一次 刷新页面才管用，点击不行
    // componentDidMount(){
    //     if(this.props.type ===1){
    //         console.log('正在')
    //         // 请求正在热映数据
    //     }else{
    //         console.log('即将')
    //         // 请求即将热映数据
    //     }
    // }
    componentWillReceiveProps(nextProps){
            // if(this.props.type ===1){
                if(nextProps.type ===1){
                console.log('正在')
                // 请求正在热映数据
            }else{
                console.log('即将')
                // 请求即将热映数据
            }
    }
    render(){
        return(
            <div>
                FilmList-{this.props.type}
            </div>
        )
    }
}
