import React, { Component } from 'react'

class Child extends Component {
    componentDidMount(){
        window.onresize=()=>{
            console.log('onresize')
        }

         this.timer= setInterval(() => {
            console.log('计时器111')
            
        }, 1000);
    }
    //在删除组件之前进行清理操作，比如计时器和事件监听器   事件监听解绑
      componentWillUnmount() {
        window.onresize=null
        // 清除定时器
        clearInterval(this.timer)

    }
    render() {
        return (
            <div>
                Child
            </div>
        )
    }
}
export default class App extends Component {
    state = {
        isCreated: true
    }
  
    render() {
        return (
            <div>
                <button onClick={()=>{
                    this.setState({
                        isCreated: !this.state.isCreated
                    })
                }}>Click</button>
                {/* {this.state.isCreated ? <Child /> : ''} */}
                {this.state.isCreated && <Child />}
            </div>
        )
    }
}
