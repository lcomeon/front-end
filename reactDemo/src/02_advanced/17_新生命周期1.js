import React, { Component } from 'react'

export default class App extends Component {
  state = {
    myname: "limiao"

  }
  // 第一次初始化组件以及后续的更新过程中（包括自身状态更新以及父传子），返回一个对象作为新的state，
  // 返回null则说明不需要在这里更新state
  // static  是静态属性 不能使用this
  // ---只负责转状态----
  // componentWillReceiveProps   父组件更新就会调用  应该避免不必要的更新
  static getDerivedStateFromProps(nextProps, nextState) {
    console.log('getDerivedStateFromProps')
    return {
      // myname:'Limiao',
      // myage:'20'//state  中没有的会被添加，有的则覆盖
      myname: nextState.myname,
    }
  }
  // componentDidUpdate  也不能进行状态更新，也会引发死循环问题
  componentDidUpdate(prevProps,prevState){
    if(this.state.myname === prevState.myname){
      return
    }
  }
  render() {
    return (
      <div>
        <button onClick={() => {
          this.setState({
            myname: '李淼'
          })
        }}>Click</button>
        {/* {this.state.myname}-{this.state.myage} */}
        {this.state.myname}
      </div>
    )
  }
}
