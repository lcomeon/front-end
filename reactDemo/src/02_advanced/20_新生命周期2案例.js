import React, { Component } from 'react'
import './css/04-clear.css'

export default class App extends Component {
  state={
    list:[1,2,3,4,5,6,7,8]
  }
  myref=React.createRef()
  getSnapshotBeforeUpdate(){
    // 获取容器的高度
    console.log(this.myref.current.scrollHeight)
    return this.myref.current.scrollHeight
  }
  componentDidMount(prevProps,prevState,value){
    console.log(this.myref.current.scrollHeight)
    this.myref.current.scrollTop +=this.myref.current.scrollHeight-value
  }
  render() {
    return (
      <div>
        <button onClick={()=>{
          this.setState({
            list:[...[11,12,13,14,15,16],...this.state.list]
          })
        }}>
          来邮件
        </button>
        <h1>邮箱</h1>
        <div style={{height:"200px",overflow:"auto"}} ref={this.myref}>
          <ul>
            {
              this.state.list.map((item,index)=>
                <li key={item} style={{height:'100px',background:"yellow"}}>{item}</li>
              )
            }
          </ul>
        </div>
      </div>
    )
  }
}
