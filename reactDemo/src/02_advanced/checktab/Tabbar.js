import React, { Component } from 'react'

export default class Tabbar extends Component {
    state = {
        list: [
            {
                id: 1,
                text: '电影'
            },
            {
                id: 2,
                text: '影院'
            },
            {
                id: 3,
                text: '我的'
            },
        ],
        current: this.props.defaultValue
    }
    render() {
        return (
            <div>
                <ul>
                    {
                        this.state.list.map((item, index) =>
                            <li key={item.id} className={this.state.current === index ? 'active' : ''} onClick={() => this.handleClick(index)}>{item.text}</li>
                        )
                    }
                </ul>
            </div>
        )
    }
    handleClick = (index) => {
        // console.log(index)
        this.setState({
            current: index
        })
        this.props.event(index)

        //通知父组件该修改父组件的状态了
    }
}

// 在React渲染生命周期时，表单元素上的value将会覆盖DOM节点中的值，在非受控组件中，经常希望React能赋予组件一个初始值，但是不去控制后续的更新。
// 在这种情况下可以指定一个defalutValue属性而不是value
// React组件的数据渲染是否被调用者传递的props完全控制，控制则为受控组件，否则为非受控组件