import React,{useState,useEffect, useLayoutEffect} from 'react'
export default function App() {
    const [myname, setmyname] = useState("miao")

    useEffect(()=>{
    //    setmyname("哈哈")
    setmyname(myname.substring(0,1).toUpperCase()+myname.substring(1))
    },[myname]) 


    // useEffect(() => {
    //     // axios.get()
    // }, [])   // 传空数组，如果使用了变量但没有在依赖中声明，当依赖的变量改变时useEffect也不会再次执行 

    return (
        <div>
            {myname}
          <button onClick={()=>{
            setmyname("hhhh")
          }}>add</button>
        </div>
    )
}


// useEffect 和 useLayoutEffect
// 都是 React中的Hook，用于在函数组件中执行副作用操作
// 他们之间的区别在于执行时机不同，useEffect在组件渲染完成之后异步执行，而useLayoutEffect在组件渲染之后同步执行
// 具体说：
// useEffect 会在组件渲染之后异步执行副作用操作，不会阻塞组件的渲染过程，因此对页面性能没有影响；
// 而useLayoutEffect则会在组件渲染之后同步执行副作用操作，会阻塞组件的渲染过程，因此可能会影响页面性能
// 另外，由于 useLayoutEffect是同步执行的，在其中修改DOM元素的样式或属性可以立即生效，
// 而在useEffect中修改则需等到下一次渲染时才能生效。

