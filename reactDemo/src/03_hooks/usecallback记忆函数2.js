import React, { useState,useCallback } from 'react'

export default function App() {
  const [text,settext]=useState("")
  const [list,setList]=useState(["aa","bb","cc"])

  // 这样handleChange函数可以被缓存下来
  const handleChange=useCallback(
    (evt)=>{
      settext(evt.target.value)
    },[]
  )
  const handleAdd=useCallback(
    ()=>{
      console.log(text)
      setList([...list,text])
      settext("")
    },[list,text]
  )
  const handleDel=useCallback(
    (index)=>{
      console.log(index)
      var newlist=[...list]
      newlist.splice(index,1)
      setList(newlist)
    },[list]
  )
  return (
    <div>
      <input onChange={handleChange} value={text}/>
      <button onClick={handleAdd}>add</button>
      <ul>
        {
          list.map((item,index)=>{
            <li key={item}>
              {item}
              <button onClick={()=>handleDel(index)}>del</button>
            </li>
          })
        }
      </ul>
      {!list.length && <div>暂无待办事项</div>}
    </div>
  )
}
