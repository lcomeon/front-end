//从React的包当中引入了React，只要写React.js组件就必须引入React，因为React里有一种语法叫JSX
//ReactDOM可以把React组件渲染到页面上去，没有其他作用了，是从react-dom中引入的，而不是从react引入
import React from "react";//17之后引不引都行
import ReactDOM  from "react-dom";
// import App from "./01_base/01_class组件";
import App from "./03_hooks/05_useLayoutEffect"

// ReactDOM.render('<h1>222</h1>',document.getElementById('root'))//标签以字符形式渲染了，加上引号反而不正常解析
// ReactDOM.render(<h1>222</h1>,document.getElementById('root'))//正常渲染
ReactDOM.render(<App />,document.getElementById('root'))//正常渲染

// 不提倡使用
// ReactDOM.render(React.createElement('div',{
//   id:'a'
// },'111'),document.getElementById('root'))
// ReactDOM.render